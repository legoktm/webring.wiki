# [webring.wiki](https://webring.wiki)

A wiki-themed webring using [go-webring](https://sr.ht/~amolith/go-webring).

## How-to
### Modifying members

Edit the `members:` object in `deployment/webring-wiki.yaml`.
The ID (object key) should be a single lowercase string of alphanumeric
characters or dashes, no spaces please.

Any changes committed to the main branch of this repository will be
deployed automatically.

### Modifying the HTML front page template
Edit `charts/webring-wiki/index.html`, then bump the chart version in
`charts/webring-wiki/Chart.yaml`. Again, changes committed to main will
be deployed automatically.

## License

Like go-webring itself, this deployment configuration is also licensed
under BSD-2-Clause.

Copyright (c) 2024 Taavi Väänänen
